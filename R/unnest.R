# Let <from> be a column of type "list". unnest returns a dataframe with as many
#     columns as nested keys in <from>, as would base::unlist do.
# Example : if the template for <from> is
#     list(a = list(<val>),
#         b = list(b1 = <val>,
#             b2 = list(b21 = <val>,
#                 b22 = <val>)
#             )
#         )
#     Then the columns a, b.b1, b.b2.b21, b.b2.b22 will be created.
unnest <- function(dataframe, from) {
    .logger$DEBUG("Entering", "unnest")
    .logger$WARNING("Not yet implemented:", "unnest")
    firstval <- base::eval(base::parse(text=as.character(dataframe[[from]][1])))
    result <- list()
}
