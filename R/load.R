#' Convenient method to help you load many .Rdata files into a single data frame or a list of data frames
#'
#' Each file must contain one single data frame (other cases not handled yet). Each data frame is loaded and added to a list.
#'
#' If \code{concatenate == TRUE}, the elements of the list are merged using \code{\link{dataframes-rbind}}.
#'
#' @param ... .Rdata files containing each data frame
#' @param concatenate If \code{TRUE}, then merges all loaded data frames into one single data frame. If \code{FALSE}, returns a list of data frames.
#' @return a single data frame merging all the data from passed arguments.
#' @examples
#' a <- data.frame()
#' b <- data.frame(first = c(11, 12), second = c(21, 22))
#' c <- data.frame(second = c(21, 22), third = c(31, 32))
#' load(a, b, c)
#'   first second third
#' 1    11     21    NA
#' 2    12     22    NA
#' 3    NA     21    31
#' 4    NA     22    32
#' @name load
load <- function(..., concatenate = TRUE) {
    dirFiles <- unlist(list(...))
    if ( (is.null(dirFiles)) || (length(dirFiles) == 0) ) {
        .logger$WARNING("Empty list of files, returning NULL.")
        return(NULL)
    }
    returned <- list()
    lineCount <- 0
    eltIndex <- 1
    for (someFile in dirFiles) {
        .logger$INFO("Loading", someFile)
        dataframe <- base::load(someFile) %>% as.name %>% eval
        dfLineCount <- utilDataframes:::nrow(dataframe)
        lineCount <- lineCount + dfLineCount
        .logger$DEBUG(someFile, "has", dfLineCount, "lines. Total number of lines is", lineCount)
        # Putting loaded data in a list
        returned[[eltIndex]] <- dataframe
        # Concatenating if need be
        if ( (concatenate) && (length(returned) >= 2) ) {
            returned <- list(utilDataframes:::rbind(returned[[1]], returned[[2]]))
        }
        eltIndex <- length(returned) + 1
    }
    # Concatenated
    if (concatenate) {
        .logger$INFO("Returning one file with", utilDataframes:::nrow(returned[[1]]), "lines. Expected:", lineCount)
        return(returned[[1]])
    }
    .logger$INFO("Returning", length(returned), "files with", lineCount, "lines.")
    return(returned)
}
