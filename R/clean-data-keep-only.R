CLEAN_DATA$KEEP_ONLY <- function(...) {
    args <- unlist(list(...))
    function(dataframe) {
        return(
            utilDataframes:::dropColumns(
                dataframe = dataframe,
                # Drop everything that is not a in ...
                dropped = dplyr::setdiff(names(dataframe), args)
            )
        )
    }
}
