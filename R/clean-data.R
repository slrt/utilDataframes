CLEAN_DATA <- list()
# Does nothing on a dataframe
CLEAN_DATA$DO_NOTHING = function() {
    return(function(dataframe) dataframe)
}
# Adds a row number
CLEAN_DATA$ADD_ROW_NUMBER = function(name) {
    force(name)
    function(dataframe) {
        return(
            dataframe %>%
                dplyr::mutate(name = dplyr::row_number())
        )
    }
}
# Removes the first n rows
CLEAN_DATA$REMOVE_FIRST_ROWS = function(nOfRows) {
    force(nOfRows)
    function(dataframe) {
        if (nOfRows == 0) {
            return(dataframe)
        }
        return(dataframe[-(1:nOfRows),])
    }
}
# nth row as header
CLEAN_DATA$NTH_ROW_AS_HEADER = function(rownum) {
    force(rownum)
    function(dataframe) {
        if (rownum == 0) {
            return(dataframe)
        }
        colnames(dataframe) <- as.character(unlist(dataframe[rownum,]))
        return(dataframe[-(rownum),])
    }
}
