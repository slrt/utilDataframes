CLEAN_DATA$cast_factor_to_text = as.character
CLEAN_DATA$cast_factor_to_numeric = pryr::compose(as.numeric, as.character)
CLEAN_DATA$cast_factor_to_boolean = pryr::compose(as.logical, as.character)
CLEAN_DATA$cast_factor_to_logical = CLEAN_DATA$cast_factor_to_boolean
CLEAN_DATA$cast_factor_to_integer = pryr::compose(as.integer, as.character)
# Not working
CLEAN_DATA$cast_factor_to_date = as.character#function(format = "%d/%m/%Y %H:%M:%S") {force(format); function(x) strptime(as.character(x), format)}
CLEAN_DATA$CAST_COLUMNS <- function(columns, as.what) {
    force(columns)
    force(as.what)
    function(dataframe) {
        require(dplyr)
        gcols <- list()
        gaswh <- list()
        for (i in 1:length(columns)) {
            if (is.null(dataframe[[columns[[i]]]])) {
                .logger$WARNING("Column", columns[[i]], "does not exist.")
            } else {
                .logger$TRACE("Dispatching column", columns[[i]])
                fnId <- utilCollections::retrieve(gaswh, element = as.what[[i]])
                if (!fnId) {
                    .logger$TRACE("New process found with number", fnId)
                    fnId <- length(gaswh) + 1
                    gcols[[
                        fnId
                        ]] <- c(columns[[i]])
                    gaswh[[
                        fnId
                        ]] <- as.what[[i]]
                } else {
                    gcols[[ fnId ]] <- c(
                        gcols[[ fnId ]],
                        columns[[i]]
                    )
                }
            }
        }
        for (i in 1:length(gaswh)) {
            .logger$TRACE("Treating columns to be processed by", i, "th process.")
            dataframe[,gcols[[i]]] <- sapply(
                dataframe[,gcols[[i]]],
                gaswh[[i]])
        }
        return(dataframe)
    }
}
