CLEAN_DATA$DROP_ONLY <- function(...) {
    force(...)
    args <- unlist(list(...))
    function(dataframe) {
        require(dplyr)
        return(
            utilDataframes:::dropColumns(
                dataframe,
                # Drop everything that is not a name of dataframe
                dropped = args
            )
        )
    }
}
